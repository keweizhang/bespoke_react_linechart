var config = {
   entry: './main.js',
   
   output: {
      // path:'/Users/keweizhang/Documents/reactApp/',
      path :'absolute-path-to-project-folder'
      filename: 'index.js',
   },
   
   devServer: {
      inline: true,
      port: 8080,
   },
   
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude:/(node_modules|bower_components)/,
            loader: 'babel-loader',
            
            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   }
}

module.exports = config;

