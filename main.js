"use strict"

var React = require('react');
var ReactDOM = require('react-dom');
var LineChart = require('react-d3-basic').LineChart;

(function() {
  // load json format EAR data
  var chartData = require('json-loader!./EAR_data/EAR.json');

  // parse date data into js readable date format
  var parseDate = d3.time.format("%Y-%m-%d").parse;

  var title = "EAR",
    width = 500,
    height = 300,
    margins = {left: 100, right: 100, top: 50, bottom: 50},
    chartSeries = [
      {
        field: 'effective annual rate',
        name: 'effective annual rate',
        color: '#ff7f0e'
      }
    ],
    x = function(d) {
      return parseDate(d.date.slice(0,10));
    },
    xScale = 'time';


  ReactDOM.render(
    <LineChart
      margins= {margins}
      data={chartData}
      width={width}
      height={height}
      chartSeries={chartSeries}
      x={x}
      xScale={xScale}
    />
  , document.getElementById('line-user')
  )
})()