Bespoke Coding Challenge - React.js Charting

This project is a visualization division of original Bespoke Coding Challenge 2017. 
The project read exported json format EAR and chart the data in web page using React.js.


Prerequisites

see package.json file for dependencies.


Installing

Install all packages listed in package.json file
$ npm install 'package-name' --save

Change 'path' in webpack.config.js file to the absolute path in your server.

For running the server
$ npm start

